/*
  main.cpp
  Main code where the control takes place
  @author  Leo Korbee (c), Leo.Korbee@xs4all.nl
  @website iot-lab.org
  @license Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
  Thanks to all the folks who contributed beforme me on this code.

  Hardware information at the end of this file.
  @version 2018-03-31

  @version 2018-06-21
  binary was broken on atmelavr platform 1.9.0 (7200 bytes hex file)
  modified platform.ini to force to platform 1.8.0 (7212 bytes hex file)

  @version 2019-08-04
  Version that uses a DS18B20 sensor
  Using OneWire library and DallasTemperature library

  @version 2021-04-14
  Due to changes on the Things Stack V3 use the define in LoRaWAN.cpp
  Read the post on https://www.iot-lab.org/blog/663/ for seting up this ABP node
  on the Things Stack.

  @version 2021-04-14
  Redefined pin numbering and hence numbering for module and sensor.

*/
#include <Arduino.h>
#include <avr/sleep.h>
#include <avr/wdt.h>
#include "tinySPI.h"
#include "LoRaWAN.h"
#include "secconfig.h" // remember to rename secconfig_example.h to secconfig.h and to modify this file
// for using the DS18B20 sensor
#include <OneWire.h>
#include <DallasTemperature.h>

// Redefine Pin numbers due to new Arduino Core/framework
// Counterclockwise numbering, checked with blink test
// .platformio/packages/framework-arduino-avr-attiny/cores/tiny/Arduino.h
#define PB0 0
#define PB1 1
#define PB2 2
#define PA7 3
#define PA6 4
#define PA5 5
#define PA4 6
#define PA3 7
#define PA2 8
#define PA1 9
#define PA0 10

// RFM95W
#define DIO0 0 // PB0
#define NSS  1  // PB1
RFM95 rfm(DIO0, NSS);

// DS18B20 // PA7 = arduino pin 7
#define ONE_WIRE_BUS 3
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature ds18b20_sensors(&oneWire);

// define LoRaWAN layer
LoRaWAN lora = LoRaWAN(rfm);
// frame counter for lora
unsigned int Frame_Counter_Tx = 0x0000;

// the things network stuff
// get them from the device overview page! uncomment and put this information here, remove the #include secconfig.h
// due to security reasons this information from the author is put in secconfig.h

// TTN, msb left
// unsigned char NwkSkey[16] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
// unsigned char AppSkey[16] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
// unsigned char DevAddr[4] = { 0x00, 0x00, 0x00, 0x00 };

// sleep cycles that will be counted, start with more than sleep_total to start after 8 seconds with first broadcast.
volatile int sleep_count = 38;

// set sleep time between broadcasts. The processor awakes after 8 seconds deep-sleep_mode,
// increase and checks the counter and sleep again until sleep_total is reached.
// 5min * 60s = 300/8 = 37,5 = 38.
// 17 seconds longer than 5 minutes with 37, so 35 is more apropriate
const int sleep_total = 35; // was 35

// all functions declared
ISR(WDT_vect);
uint16_t vccVoltage();
void setUnusedPins();
void goToSleep();
void watchdogSetup();


void setup()
{
  // define unused pins
  setUnusedPins();

  // initialize RFM module
  rfm.init();

  lora.setKeys(NwkSkey, AppSkey, DevAddr);

  // initialize DS18B20
  ds18b20_sensors.begin();

}

void loop()
{

  // goToSleep for all devices...
  // The watchdog timer will reset.
  goToSleep();

  // use this for non-sleep testing:
  //delay(8000);
  //sleep_count++;

  // do action if sleep_total is reached
  if (sleep_count >= sleep_total)
  {
    uint16_t tempInt;

    // define bytebuffer
    uint8_t Data_Length = 4;
	  uint8_t Data[Data_Length];

    float temp;

    // Send the command to get temperatures
    ds18b20_sensors.requestTemperatures();
    temp = ds18b20_sensors.getTempCByIndex(0);

    if(temp == DEVICE_DISCONNECTED_C)
    {
      // error
    }

    tempInt = temp * 100;

    // read vcc voltage (mv)
    uint16_t vcc = vccVoltage();
    Data[0] = (vcc >> 8) & 0xff;
    Data[1] = (vcc & 0xff);

    // move into bytebuffer
    Data[2] = (tempInt >> 8) & 0xff;
    Data[3] = tempInt & 0xff;

    lora.Send_Data(Data, Data_Length, Frame_Counter_Tx);

    Frame_Counter_Tx++;

    // reset sleep count
    sleep_count = 0;

  }

}


/*
  set unused pins so no undefined situation takes place
*/
void setUnusedPins()
{
  // 0, 1, 2, 3, 8
  pinMode(0, INPUT_PULLUP);
  pinMode(1, INPUT_PULLUP);
  pinMode(2, INPUT_PULLUP);
  pinMode(3, INPUT_PULLUP);
  pinMode(8, INPUT_PULLUP);
}


/**
  read voltage of the rail (Vcc)
  output mV (2 bytes)
*/
uint16_t vccVoltage()
{
  // Read 1.1V reference against AVcc
  // set the reference to Vcc and the measurement to the internal 1.1V reference
  // default ADMUX REFS1 and REFS0 = 0

  // #define _BV(bit) (1 << (bit))

  // 1.1V (I Ref)(2) 100001
  ADMUX = _BV(MUX5) | _BV(MUX0);

  delay(2); // Wait for Vref to settle
  ADCSRA |= _BV(ADSC); // Start conversion
  while (bit_is_set(ADCSRA,ADSC)); // measuring

  uint8_t low  = ADCL; // must read ADCL first - it then locks ADCH
  uint8_t high = ADCH; // unlocks both

  uint16_t result = (high<<8) | low;

  // result = 1125300L / result; // Calculate Vcc (in mV); 1125300 = 1.1*1023*1000
  // number of steps = 1023??
  result = (1125300L / result) ; // Calculate Vcc (in mV);

  return result;
}

void watchdogSetup()
{
  // Table for clearing/setting bits
  //WDP3 - WDP2 - WPD1 - WDP0 - time
  // 0      0      0      0      16 ms
  // 0      0      0      1      32 ms
  // 0      0      1      0      64 ms
  // 0      0      1      1      0.125 s
  // 0      1      0      0      0.25 s
  // 0      1      0      1      0.5 s
  // 0      1      1      0      1.0 s
  // 0      1      1      1      2.0 s
  // 1      0      0      0      4.0 s
  // 1      0      0      1      8.0 s


  // Reset the watchdog reset flag
  bitClear(MCUSR, WDRF);
  // Start timed sequence
  bitSet(WDTCSR, WDCE); //Watchdog Change Enable to clear WD
  bitSet(WDTCSR, WDE); //Enable WD

  // Set new watchdog timeout value to 8 second
  bitSet(WDTCSR, WDP3);
  bitClear(WDTCSR, WDP2);
  bitClear(WDTCSR, WDP1);
  bitSet(WDTCSR, WDP0);
  // Enable interrupts instead of reset
  bitSet(WDTCSR, WDIE);
}

void goToSleep()
{
  //Disable ADC, saves ~230uA
  ADCSRA &= ~(1<<ADEN);
  watchdogSetup(); //enable watchDog
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
  // sleep_mode does set and reset the SE bit. sleep_enable and sleep_disable are not usefull
  sleep_mode();
  //disable watchdog after sleep
  wdt_disable();
  // enable ADC
  ADCSRA |=  (1<<ADEN);
}


ISR(WDT_vect)
{
  sleep_count++; // keep track of how many sleep cycles have been completed.
}


/*
  Hardware setup
  Attiny84 using the Arduino pin numbers! PB0 = 0 etc.

Atmel ATtiny84A PU
  RFM95W
  DS18B20 Dallas Temperature sensor

  Power: 3V3 - 470uF elco over power connectors, 100nF over power connector for interference suppression
  Connections:
  RFM95W   ATtiny84

  DIO0 -- PB0
  MISO -- MOSI
  MOSI -- MISO
  SCK  -- SCK
  NSS  -- PB1 (this is Chipselect)

  DS18B20 sensors connections

  DS18B20  ATtiny84
  DQ -- PA7

*/

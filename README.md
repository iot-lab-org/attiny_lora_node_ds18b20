ATtiny84 LoRa node with DS18B20 Temperature sensor
==================================================

Hardware
--------
- ATtin84A PU
- RFM95W LoRa modem
- DS18B20 Temperature sensor
- 3 to 3V3 power supply/battery


Software
--------
Using PlatformIO as development platform


for further information see https://www.iot-lab.org and comments in source files.
